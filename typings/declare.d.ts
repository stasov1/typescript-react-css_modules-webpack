declare function require(path: string): any;
declare module "react-hot-loader";

interface RequireImport {
    default: any;
}