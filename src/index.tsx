import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from "react-hot-loader";

import App from './containers/App';

const _render = (Component: React.ReactNode) => {
  ReactDOM.render(
    <AppContainer>
      {Component}
    </AppContainer>,
    document.getElementById('app') as HTMLDivElement
  );
}

_render(<App />);

if (module.hot) {
  module.hot.accept('./containers/App', () => {
    const NextApp = require("./containers/App").default;
    _render(<NextApp />);
  });
}
