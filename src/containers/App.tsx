import * as React from 'react';

import Button from '../components/Button';

const styles = require('./App.css');

const App = () => {
  return (
    <div className={styles.container}>
      <Button className={styles.button}>
        ololo
      </Button>
    </div>
  );
}

export default App;
