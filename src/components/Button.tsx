import * as React from 'react';

interface IButton {
  [key: string]: any
}

const Button = (props: IButton) => <button {...props} />;

export default Button;
